
void loop_nano() {

  if(water_state == false && pump_state == false && pump_fail_state == false && water_fail_state == false){
  
    if(digitalRead(level_control) == HIGH) {
      get_mix();
      send_message("save|" + String(id), "no");
    }
          
  }

  else{

    check_alarms();

  }

  if(digitalRead(config_button) == HIGH)
    send_message("config|" + String(id), "no");

  extract_info(get_message());
  check_info_and_do_something();

}
