String get_message() {

    long int timeout = millis();

    while(!Serial.available()) {

      if((millis() - timeout) > 2000)
        return "";

    }

    char inChar = NULL;
    String message = "";

    while(Serial.available()) {
      inChar = Serial.read();
      message = message + inChar;
      long int timeout = millis();
      while((millis() - timeout) < 10) {}
    }

    return message;

}

String send_message(String msg, String wait_response) {

  Serial.flush();
  Serial.println(msg);

  if(wait_response == "yes")
    return get_message();

  return "";

}

void extract_info(String msg) {

  if(msg != ""){

    int fstPos = msg.indexOf('|');
    info[0] = msg.substring(0, fstPos);
    info[1] = msg.substring(fstPos + 1);

  }

}

void advise() {

  tone(buzzer, 1000);
  long int now = millis();

  while(true) {
    if((millis() - now) > 500)
      break;
  }

  noTone(buzzer);

}

void check_info_and_do_something() {

  if(info[0] == "init") {
    EEPROM.update(id_addr, info[1].toInt());
    send_message("done", "no");
    info[0] = "none";
    advise();
  }

  if(info[0] == "confwater") {

    EEPROM.update(wpulses_addr, info[1].toInt());
    water_pul = info[1].toInt();
    send_message("done", "no");
    info[0] = "none";
    advise();
  }

  if(info[0] == "confsyrup") {
    EEPROM.update(spulses_addr, info[1].toInt());
    pump_obj= info[1].toInt();
    send_message("done", "no");
    info[0] = "none";
    advise();
  }

  if(info[0] == "confprop") {
    EEPROM.put(proportion_addr, info[1].toFloat());
    proportion = info[1].toFloat();
    send_message("done", "no");
    info[0] = "none";
    advise();
  }

  if(info[0] == "getid"){
    send_message("init|"+ String(id), "yes");
    info[0] = "none";
    advise();
  }

  if(info[0] == "testwater") {
    advise();
    get_water(true);
    info[0] = "none";
    advise();
  }

  if(info[0] == "testsyrup") {
    advise();
    get_syrup();
    info[0] = "none";
    advise();
  }

  if(info[0] == "testprop") {
    advise();
    get_mix();
    info[0] = "none";
    advise();
  }

  long int now = millis();

  while(true) {
    if((millis() - now) > 50)
      break;
  }

}
