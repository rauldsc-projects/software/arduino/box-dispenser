// Define the directions in EEPROM
#define id_addr 0
#define wpulses_addr 2
#define spulses_addr 4
#define proportion_addr 6
#define water_fail_addr 10
#define syrup_fail_addr 12

// Define pins for flowmeters
#define q_water 2
#define q_pump 3

// Define pins for level control
#define level_control 4

// Define pins for actuators
#define led_ward 5
#define pump 6
#define electro 7
#define alarm_button 10
#define config_button 11

// Define pins for indicators
#define led_fpump 8
#define led_fwater 9
#define buzzer A0
#define guard A1

// Declare volatile variables for interrupts
volatile int water_pulses;
volatile int pump_pulses;
volatile bool water_state;
volatile bool pump_state;
volatile long int starting_time_water;
volatile long int starting_time_pump;
volatile int water_quantity;

int id = 0;
String info[2];
float proportion = 0;
int water_obj = 0;
int water_prop = 0;
int pump_obj = 0;
int water_fail = 0;
int syrup_fail = 0;
int water_pul = 0;


bool pump_fail_state = false;
bool water_fail_state = false;
long int starting_time_fail;

unsigned long int overtime_water = 120000;
unsigned long int overtime_pump = 15000;
unsigned long int overtime_fail = 10000;
