void count_water() {

  water_pulses++;
  
  if(water_pulses >= water_obj){

    digitalWrite(electro, LOW);
    EEPROM.update(water_fail_addr, 0);
    water_state = false;

  }

}

void count_pump() {

  pump_pulses++;
  
  if(pump_pulses >= pump_obj){

    digitalWrite(pump, LOW);
    EEPROM.update(syrup_fail_addr, 0);
    pump_state = false;

  }

}
