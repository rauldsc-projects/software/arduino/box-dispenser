
void setup_nano() {

  Serial.begin(9600);

  id = EEPROM.read(id_addr);
  water_pul = EEPROM.read(wpulses_addr);
  pump_obj = EEPROM.read(spulses_addr);
  proportion = EEPROM.get(proportion_addr, proportion);
  water_fail = EEPROM.read(water_fail_addr);
  syrup_fail = EEPROM.read(syrup_fail_addr);
  pinMode(q_water, INPUT);
  pinMode(q_pump, INPUT);
  pinMode(alarm_button, INPUT);
  pinMode(config_button, INPUT);

  pinMode(pump, OUTPUT);
  digitalWrite(pump, LOW);
  pinMode(electro, OUTPUT);
  digitalWrite(electro, LOW);
  pinMode(led_fpump, OUTPUT);
  digitalWrite(led_fpump, LOW);
  pinMode(led_fwater, OUTPUT);
  digitalWrite(led_fwater, LOW);
  pinMode(led_ward, OUTPUT);
  digitalWrite(led_ward, LOW);
  noTone(buzzer);

  String response = send_message("init|" + String(id), "yes");
  extract_info(response);
  check_info_and_do_something();

  attachInterrupt(0, count_water, RISING);
  attachInterrupt(1, count_pump, RISING);

  if(water_fail == 1)
    get_water(false);

  if(syrup_fail == 1)
    get_syrup();

}
