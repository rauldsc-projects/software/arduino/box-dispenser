
void get_water(bool test_water) {

  digitalWrite(electro, HIGH);
  digitalWrite(led_fwater, LOW);
  noTone(buzzer);
  noInterrupts();

  if(test_water){
    water_obj = water_pul;
  }else{
    water_obj = (int)(water_pul * proportion);
  }
  water_pulses = 0;
  water_state = true;
  starting_time_water = millis();

  interrupts();

}

void get_syrup() {

  digitalWrite(pump, HIGH);
  digitalWrite(led_fpump, LOW);
  noTone(buzzer);
  noInterrupts();

  pump_pulses = 0;
  pump_state = true;
  starting_time_pump = millis();

  interrupts();

}

void get_mix() {
  get_water(false);
  get_syrup();
}

void check_alarms() {

  if(pump_state == true && pump_fail_state == false && (millis() - starting_time_pump) > overtime_pump){

    digitalWrite(pump, LOW);
    digitalWrite(led_fpump, HIGH);
    pump_fail_state = true;
    EEPROM.update(syrup_fail_addr, 1);
    tone(buzzer,1000);

  }


  if(water_state == true && water_fail_state == false && (millis() - starting_time_water) > overtime_water){

    digitalWrite(electro, LOW);
    digitalWrite(led_fwater, HIGH);
    water_fail_state = true;
    starting_time_fail = millis();
    EEPROM.update(water_fail_addr, 1);

  }

  if(pump_fail_state == true){
    if(digitalRead(led_fpump) == LOW)
      digitalWrite(led_fpump, HIGH);
    else
      digitalWrite(led_fpump, LOW);
  }

  if(water_fail_state == true){
    if(digitalRead(led_fwater) == LOW)
      digitalWrite(led_fwater, HIGH);
    else
      digitalWrite(led_fwater, LOW);
  }

  if((pump_fail_state == true) && (digitalRead(alarm_button) == LOW)){

    digitalWrite(pump, HIGH);
    digitalWrite(led_fpump, LOW);
    pump_fail_state = false;
    starting_time_pump = millis();
    noTone(buzzer);
    
  }


  if((water_fail_state == true) && (millis() - starting_time_fail) > overtime_fail){

    digitalWrite(electro, HIGH);
    digitalWrite(led_fwater, LOW);
    water_fail_state = false;
    starting_time_water = millis();
    
  }  

}
