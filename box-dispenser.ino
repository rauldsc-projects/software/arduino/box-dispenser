#include <EEPROM.h>
#include "Variables.h"
#include "InterruptFunctions.h"
#include "DispenseFunctions.h"
#include "CommunicationFunctions.h"
#include "Setup.h"
#include "Loop.h"

void setup() {setup_nano();}

void loop() {loop_nano();}
